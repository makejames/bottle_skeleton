SHELL=/bin/bash
EXECUTE=poetry run
VERSION=$$(cat src/VERSION)

# code
PACKAGE_NAME=bottle_skeleton
PACKAGE=src

# file groups
TEST_GROUP=tests/test_*.py
LINT_GROUP=$(PACKAGE)
CLEAN_GROUP=$(PACKAGE) tests/

# targets
## standard
all: build

install:
	python3 -m pip install ./dist/$(PACKAGE_NAME)-$(VERSION)-py3-none-any.whl

uninstall:
	python3 -m pip uninstall $(PACKAGE_NAME)

clean:
	rm -rf dist

info:

check: format lint test performance

## less standard
dev:
	poetry install
	poetry env use python3
	cp pre-commit .git/hooks/

pre-commit: lint

format:
	$(EXECUTE) isort $(LINT_GROUP)

lint:
	$(EXECUTE) pydocstyle $(LINT_GROUP) $(TEST_GROUP)
	$(EXECUTE) pycodestyle $(LINT_GROUP) $(TEST_GROUP)
	$(EXECUTE) pyflakes $(LINT_GROUP)
	$(EXECUTE) bandit -r $(LINT_GROUP) -q
	$(EXECUTE) mypy $(LINT_GROUP)

test:
	$(EXECUTE) pytest $(TEST_GROUP) 


coverage:
	$(EXECUTE) pytest --cov=$(PACKAGE) $(TEST_GROUP) --cov-report term-missing

