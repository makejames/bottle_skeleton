"""Create the application routes."""

from bottle import Bottle, run
from os import path


app = Bottle()
app_name = "Bottle Skeleton"
app_version = open(path.join(path.dirname(__file__), "VERSION"), "rt").read()


@app.route("/")
def root():
    """Provide a root end point with app and version number."""
    return f"{app_name}: {app_version}"


if __name__ == "__main__":
    run(app, host='127.0.0.1', port=8080)
